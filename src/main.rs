use std::{
    fs::{self, File},
    path::PathBuf,
    process::exit,
};

use log::{debug, error, trace, LevelFilter};
use structopt::StructOpt;

use compiler::Chunk;
use lexer::TokenKind;

mod compiler;
mod eval;
mod lexer;
mod parser;

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(subcommand)]
    command: Command,
    #[structopt(short, long, default_value = "Info")]
    log_level: LevelFilter,
}

#[derive(StructOpt, Debug)]
enum Command {
    #[structopt(name = "compile")]
    Compile {
        #[structopt(parse(from_os_str))]
        input: PathBuf,
        #[structopt(parse(from_os_str))]
        output: PathBuf,
    },
    #[structopt(name = "run")]
    Run {
        #[structopt(parse(from_os_str))]
        input: PathBuf,
    },
}

fn main() {
    let opt = Opt::from_args();
    pretty_env_logger::formatted_builder()
        .filter_level(opt.log_level)
        .init();
    debug!("{:#?}", &opt);

    match opt.command {
        Command::Compile { input, output } => compile_to_file(input, output, opt.log_level),
        Command::Run { input } => run(input, opt.log_level),
    };
}

fn compile_to_file(input: PathBuf, output: PathBuf, log_level: LevelFilter) {
    let chunk = compile(input, log_level);
    let file = match File::create(output) {
        Ok(file) => file,
        Err(err) => {
            error!("Failed to create file: {}", err);
            exit(1);
        }
    };
    if let Err(err) = chunk.write_chunk(file) {
        error!("Failed to write chunk to file: {}", err);
        exit(1);
    }
}

fn run(input: PathBuf, log_level: LevelFilter) {
    let chunk = match input.extension().and_then(|e| e.to_str()) {
        Some("lr") => compile(input, log_level),
        Some("lrc") => {
            let file = match File::open(input) {
                Ok(file) => file,
                Err(err) => {
                    error!("Failed to open file: {}", err);
                    exit(1);
                }
            };
            match Chunk::read_chunk(file) {
                Ok(chunk) => chunk,
                Err(err) => {
                    error!("Failed to read chunk from file: {}", err);
                    exit(1);
                }
            }
        }
        _ => {
            error!("run can only be used on \".lr\" or \".lrc\" files!");
            exit(1)
        }
    };
    debug!("chunk:\n{:#?}", chunk);
    if let Err(err) = eval::evaluate(chunk) {
        error!("Failed to evaluate file: {}", err);
        exit(1);
    }
}

fn compile(input: PathBuf, log_level: LevelFilter) -> Chunk {
    match fs::read_to_string(input) {
        Ok(content) => match lexer::lex(&content) {
            Ok(tokens) => {
                trace!("full lexed tokens: {:#?}", tokens);
                if log_level >= LevelFilter::Debug {
                    let tokenkinds: Vec<TokenKind> =
                        tokens.iter().map(|token| token.kind.clone()).collect();
                    debug!("lexed tokens: {:#?}", tokenkinds);
                }
                match parser::parse(tokens, &content) {
                    Ok(statements) => {
                        debug!("statements: {:#?}", statements);

                        drop(content);

                        match compiler::compile(statements) {
                            Ok(chunk) => chunk,
                            Err(err) => {
                                error!("Failed to compile file: {}", err);
                                exit(1);
                            }
                        }
                    }
                    Err(err) => {
                        error!("Failed to parse file: {}", err);
                        exit(1);
                    }
                }
            }
            Err(err) => {
                error!("Failed to lex file: {}", err);
                exit(1);
            }
        },
        Err(err) => {
            error!("Failed to open file: {}", err);
            exit(1);
        }
    }
}
