use std::convert::{TryFrom, TryInto};

use anyhow::{anyhow, Context, Result};
use log::trace;

use crate::compiler::{Address, Chunk, ConstIndex, OpCode, Value};

struct Vm {
    chunk: Chunk,
    stack: Vec<Value>,
    call_stack: Vec<Address>,
    locals: Vec<Value>,
    ip: Address,
}

fn constant(vm: &mut Vm) -> Result<()> {
    const INDEX_LEN: usize = (OpCode::Constant.len() - 1) as usize;
    let ip = vm.ip.try_into()?;
    let index_start = ip - INDEX_LEN;

    let const_index =
        ConstIndex::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);

    let value = vm.chunk.constant_pool[<usize>::try_from(const_index)?].clone();

    trace!(
        "loaded constant at index {:#010x} with value {:?}.",
        const_index,
        &value
    );
    vm.stack.push(value);

    Ok(())
}

fn not(vm: &mut Vm) -> Result<()> {
    let popped = vm
        .stack
        .pop()
        .context("not: failed to pop from empty stack!")?;
    trace!("notting value {:?}", popped);
    match popped {
        Value::Boolean { value } => {
            vm.stack.push(Value::Boolean { value: !value });
            Ok(())
        }
        _ => Err(anyhow!("not: can only be applied to booleans.")),
    }
}

fn negate(vm: &mut Vm) -> Result<()> {
    let popped = vm
        .stack
        .pop()
        .context("negate: failed to pop from empty stack!")?;
    trace!("negating value {:?}", popped);
    match popped {
        Value::Float { value } => {
            vm.stack.push(Value::Float { value: -value });
            Ok(())
        }
        _ => Err(anyhow!("negate: can only be applied to numbers.")),
    }
}

fn bin_op(vm: &mut Vm, opcode: OpCode) -> Result<()> {
    trace!("evaluating binary operation (opcode: {:?})", opcode);
    let rhs = vm
        .stack
        .pop()
        .context("add_rhs: failed to pop from empty stack!")?;
    let lhs = vm
        .stack
        .pop()
        .context("add_lhs: failed to pop from empty stack!")?;
    let value = match (lhs, rhs) {
        (Value::Float { value: lhs }, Value::Float { value: rhs }) => match opcode {
            OpCode::Addition => Value::Float { value: lhs + rhs },
            OpCode::Subtraction => Value::Float { value: lhs - rhs },
            OpCode::Multiplication => Value::Float { value: lhs * rhs },
            OpCode::Division => Value::Float { value: lhs / rhs },
            OpCode::Modulus => Value::Float { value: lhs % rhs },
            OpCode::GreaterEqual => Value::Boolean { value: lhs >= rhs },
            OpCode::GreaterThan => Value::Boolean { value: lhs > rhs },
            OpCode::LessEqual => Value::Boolean { value: lhs <= rhs },
            OpCode::LessThan => Value::Boolean { value: lhs < rhs },
            #[allow(clippy::float_cmp)]
            OpCode::Equal => Value::Boolean { value: lhs == rhs },
            _ => unreachable!(),
        },
        (Value::Boolean { value: lhs }, Value::Boolean { value: rhs }) => match opcode {
            OpCode::Equal => Value::Boolean { value: lhs == rhs },
            OpCode::LogicalAnd => Value::Boolean { value: lhs && rhs },
            OpCode::LogicalOr => Value::Boolean { value: lhs || rhs },
            _ => unreachable!(),
        },
        _ => {
            return Err(anyhow!(
                "binary operation {} not (yet) supported for that combination of types"
            ))
        }
    };
    vm.stack.push(value);
    Ok(())
}

fn call(vm: &mut Vm) -> Result<()> {
    const ADDRESS_LEN: usize = (OpCode::Call.len() as usize) - 1;

    let ip = vm.ip.try_into()?;
    let index_start = ip - ADDRESS_LEN;

    let fn_local_address =
        Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);
    let fn_address = match vm.locals[usize::try_from(fn_local_address)?] {
        Value::Address { value } => Ok(value),
        _ => Err(anyhow!(
            "variable with function pointer can only be of type address."
        )),
    }?;

    trace!("called function with return address {}", fn_address);

    vm.call_stack.push(vm.ip);
    vm.ip = fn_address;

    Ok(())
}

fn pop(vm: &mut Vm) -> Result<()> {
    trace!("popping from stack");
    vm.stack
        .pop()
        .context("pop: failed to pop from empty stack")?;
    Ok(())
}

fn push(vm: &mut Vm) -> Result<()> {
    const INDEX_LEN: usize = (OpCode::Push.len() - 1) as usize;
    let ip = vm.ip.try_into()?;
    let index_start = ip - INDEX_LEN;
    let address = Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);
    trace!("pushing {} to the stack", address);
    vm.stack.push(Value::Address { value: address });
    Ok(())
}

fn load(vm: &mut Vm) -> Result<()> {
    const INDEX_LEN: usize = (OpCode::Load.len() - 1) as usize;
    let ip = vm.ip.try_into()?;
    let index_start = ip - INDEX_LEN;

    let local_index = Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);

    let value = vm.locals[<usize>::try_from(local_index)?].clone();

    trace!(
        "loaded local at index {:#010x} with value {:?}.",
        local_index,
        &value
    );

    vm.stack.push(value);

    Ok(())
}

fn store(vm: &mut Vm) -> Result<()> {
    const INDEX_LEN: usize = (OpCode::Store.len() - 1) as usize;
    let ip = vm.ip.try_into()?;
    let index_start = ip - INDEX_LEN;
    let local_index = Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);

    let value = vm
        .stack
        .pop()
        .context("store: failed to pop from empty stack!")?;

    trace!(
        "stored local at index {:#010x} with value {:?}.",
        local_index,
        &value
    );

    if let Some(local) = vm.locals.get_mut(usize::try_from(local_index)?) {
        *local = value;
    } else {
        vm.locals.push(value);
        assert!(local_index == ((vm.locals.len() - 1) as u64));
    }

    Ok(())
}

fn jump_not_true(vm: &mut Vm) -> Result<()> {
    const ADDRESS_LEN: usize = (OpCode::JumpNotTrue.len() as usize) - 1;

    let ip = vm.ip.try_into()?;
    let index_start = ip - ADDRESS_LEN;

    let dest_address =
        Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);

    let boolean = vm
        .stack
        .pop()
        .context("jump_not_true: failed to pop from empty stack!")?;

    let cond = if let Value::Boolean { value } = boolean {
        value
    } else {
        return Err(anyhow!("jump_not_true only works on booleans."));
    };

    trace!(
        "called if with destination address {}; cond: {}",
        dest_address,
        cond
    );

    if !cond {
        vm.ip = dest_address;
    }

    Ok(())
}

fn jump(vm: &mut Vm) -> Result<()> {
    const ADDRESS_LEN: usize = (OpCode::Jump.len() as usize) - 1;

    let ip = vm.ip.try_into()?;
    let index_start = ip - ADDRESS_LEN;

    let dest_address =
        Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);

    trace!("called jmp with destination address {}", dest_address,);

    vm.ip = dest_address;

    Ok(())
}

fn print(vm: &mut Vm) -> Result<()> {
    const LENGTH_LEN: usize = (OpCode::Print.len() - 1) as usize;
    let ip = vm.ip.try_into()?;
    let index_start = ip - LENGTH_LEN;
    let length = Address::from_le_bytes(vm.chunk.code.as_slice()[index_start..ip].try_into()?);

    trace!("printing value");

    let mut arguments = Vec::with_capacity(length.try_into()?);
    for _ in 0..length {
        arguments.push(
            vm.stack
                .pop()
                .context("print: failed to pop from empty stack!")?,
        );
    }

    arguments.reverse();

    match arguments.get(0) {
        Some(Value::String { value: format }) => {
            // TODO: improve format printing
            let mut format = format.clone();
            for arg in arguments.iter().skip(1) {
                format = format.replace("{}", &arg.to_string());
            }
            println!("{}", format);
        }
        _ => return Err(anyhow!("first argument of print() must be string")),
    }

    vm.stack.push(Value::Void);

    Ok(())
}

pub fn evaluate(chunk: Chunk) -> Result<()> {
    trace!(
        "starting to evaluate {} bytes with {} constants",
        chunk.code.len(),
        chunk.constant_pool.len()
    );
    let mut vm = Vm {
        chunk,
        stack: Vec::new(),
        call_stack: Vec::new(),
        locals: Vec::new(),
        ip: 0,
    };

    loop {
        if let Some(opcode_byte) = vm.chunk.code.get(usize::try_from(vm.ip)?) {
            let opcode = OpCode::from(*opcode_byte);
            let length = opcode.len();
            trace!(
                "ip: {:#04x}; stack: {:#?}; ret_stack: {:#?}; locals: {:#?}; opcode: {:?}",
                vm.ip,
                &vm.stack,
                &vm.call_stack,
                &vm.locals,
                opcode
            );
            vm.ip += length as Address;
            match opcode {
                OpCode::Return => {
                    if let Some(ret_address) = vm.call_stack.pop() {
                        vm.ip = ret_address;
                    } else {
                        return Ok(());
                    }
                }
                OpCode::Constant => constant(&mut vm)?,
                OpCode::Not => not(&mut vm)?,
                OpCode::Negate => negate(&mut vm)?,
                OpCode::Addition
                | OpCode::Subtraction
                | OpCode::Multiplication
                | OpCode::Division
                | OpCode::Modulus
                | OpCode::GreaterEqual
                | OpCode::GreaterThan
                | OpCode::LessEqual
                | OpCode::LessThan
                | OpCode::Equal
                | OpCode::LogicalAnd
                | OpCode::LogicalOr => bin_op(&mut vm, opcode)?,
                OpCode::Call => call(&mut vm)?,
                OpCode::Push => push(&mut vm)?,
                OpCode::Pop => pop(&mut vm)?,
                OpCode::Load => load(&mut vm)?,
                OpCode::Store => store(&mut vm)?,
                OpCode::JumpNotTrue => r#jump_not_true(&mut vm)?,
                OpCode::Jump => jump(&mut vm)?,
                OpCode::Print => print(&mut vm)?,
            };
        } else {
            break Err(anyhow!("no instructions left but still no return"));
        }
    }
}
