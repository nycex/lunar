use anyhow::{anyhow, Result};
use log::trace;

use crate::lexer::{Token, TokenKind};

#[derive(Clone, Debug)]
pub enum Statement {
    Definition {
        variable: DefinitionVariable,
        expression: Expression,
    },
    Assigment {
        ident: String,
        expression: Expression,
    },
    Expression {
        expression: Expression,
    },
    IfCondition {
        condition: Expression,
        statements: Vec<Statement>,
        else_statements: Option<Vec<Statement>>,
    },
    WhileLoop {
        condition: Expression,
        statements: Vec<Statement>,
    },
    Return {
        expression: Option<Expression>,
    },
    Break,
    Continue,
}

#[derive(Clone, Debug)]
pub enum Expression {
    Function {
        arguments: Vec<DefinitionVariable>,
        statements: Vec<Statement>,
    },
    Block {
        statements: Vec<Statement>,
    },
    FunctionCall {
        ident: String,
        arguments: Vec<Expression>,
    },
    VariableReference {
        ident: String,
    },
    StringLit {
        string: String,
    },
    IntLit {
        uint: usize,
    },
    FloatLit {
        float: f64,
    },
    BooleanLit {
        boolean: bool,
    },
    Binary {
        operator: BinOperator,
        lhs: Box<Expression>,
        rhs: Box<Expression>,
    },
    Unary {
        operator: UnaryOperator,
        expression: Box<Expression>,
    },
}

#[derive(Clone, Debug)]
pub enum BinOperator {
    Addition,
    Subtraction,
    Multiplication,
    Division,
    Modulus,
    GreaterEqual,
    GreaterThan,
    LessEqual,
    LessThan,
    Equal,
    LogicalAnd,
    LogicalOr,
}

impl From<&TokenKind> for BinOperator {
    fn from(value: &TokenKind) -> Self {
        match value {
            TokenKind::Plus => BinOperator::Addition,
            TokenKind::Minus => BinOperator::Subtraction,
            TokenKind::Asterisk => BinOperator::Multiplication,
            TokenKind::Slash => BinOperator::Division,
            TokenKind::Percent => BinOperator::Modulus,
            TokenKind::GreaterThanEquals => BinOperator::GreaterEqual,
            TokenKind::GreaterThan => BinOperator::GreaterThan,
            TokenKind::LessThanEquals => BinOperator::LessEqual,
            TokenKind::LessThan => BinOperator::LessThan,
            TokenKind::DoubleEquals => BinOperator::Equal,
            TokenKind::DoubleAmpersand => BinOperator::LogicalAnd,
            TokenKind::DoubleVerticalLine => BinOperator::LogicalOr,
            _ => unimplemented!(),
        }
    }
}

#[derive(Clone, Debug)]
pub enum UnaryOperator {
    ExclamationMark,
    Minus,
}

impl From<&TokenKind> for UnaryOperator {
    fn from(value: &TokenKind) -> Self {
        match value {
            TokenKind::ExclamationMark => UnaryOperator::ExclamationMark,
            TokenKind::Minus => UnaryOperator::Minus,
            _ => unreachable!(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct DefinitionVariable {
    pub mutable: bool,
    pub ident: String,
    pub r#type: Type,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Type {
    Function,
    r#String,
    USize,
    ISize,
    Float,
    Boolean,
    Void,
}

impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Type::Function => write!(f, "fn"),
            Type::r#String => write!(f, "string"),
            Type::USize => write!(f, "usize"),
            Type::ISize => write!(f, "isize"),
            Type::Float => write!(f, "float"),
            Type::Boolean => write!(f, "boolean"),
            Type::Void => write!(f, "void"),
        }
    }
}

fn format_position(index: usize, input: &[Token], file_content: &str) -> String {
    let mut line = 1;
    let mut char_pos = 1;
    let take_to = if let Some(token) = input.get(index) {
        token.span.start
    } else {
        file_content.len()
    };
    for (i, chr) in file_content.char_indices().take(take_to) {
        match chr {
            '\n' => {
                line += 1;
                char_pos = 1;
            }
            '\r' => {
                if file_content.chars().nth(i + 1) != Some('\n') {
                    line += 1;
                    char_pos = 1;
                }
            }
            _ => {
                char_pos += 1;
            }
        }
    }
    format!("line {}, pos: {}", line, char_pos)
}

fn format_current(opt_token: Option<&Token>) -> String {
    if let Some(token) = opt_token {
        token.to_string()
    } else {
        "EOF".to_owned()
    }
}

fn consume<'a>(
    expected: TokenKind,
    index: &mut usize,
    input: &'a [Token],
    file_content: &str,
) -> Result<&'a Token> {
    trace!("processing consume, index: {}", index);
    if let Some(token) = input.get(*index) {
        if token.kind == expected {
            *index += 1;
            return Ok(token);
        }
    };
    Err(anyhow!(
        "{}: expected {}, found {}.",
        format_position(*index, input, file_content),
        expected,
        format_current(input.get(*index)),
    ))
}

fn peak(expected: TokenKind, index: usize, input: &[Token]) -> bool {
    trace!("processing peak, index: {}", index);
    input
        .get(index)
        .map(|token| token.kind == expected)
        .unwrap_or(false)
}

fn identifier(index: &mut usize, input: &[Token], file_content: &str) -> Result<String> {
    trace!("processing identifier, index: {}", index);
    let token = consume(TokenKind::Ident, index, input, file_content)?;
    let ident = file_content[token.span.start..token.span.end].to_owned();
    Ok(ident)
}

fn r#type(index: &mut usize, input: &[Token], file_content: &str) -> Result<Type> {
    trace!("processing type, index: {}", index);
    let r#type = match input.get(*index) {
        Some(Token {
            kind: TokenKind::r#Fn,
            span: _,
        }) => Type::Function,
        Some(Token {
            kind: TokenKind::r#String,
            span: _,
        }) => Type::r#String,
        Some(Token {
            kind: TokenKind::USize,
            span: _,
        }) => Type::USize,
        Some(Token {
            kind: TokenKind::ISize,
            span: _,
        }) => Type::ISize,
        Some(Token {
            kind: TokenKind::Float,
            span: _,
        }) => Type::Float,
        Some(Token {
            kind: TokenKind::Boolean,
            span: _,
        }) => Type::Boolean,
        Some(Token {
            kind: TokenKind::Void,
            span: _,
        }) => Type::Void,
        opt => {
            return Err(anyhow!(
                "{}: expected `fn`, `usize`, `string` or `void` (type), found {}",
                format_position(*index, input, file_content),
                format_current(opt),
            ))
        }
    };
    *index += 1;
    Ok(r#type)
}

fn function(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing function, index: {}", index);
    consume(TokenKind::r#Fn, index, input, file_content)?;
    consume(TokenKind::LParen, index, input, file_content)?;

    let mut arguments = Vec::new();
    while !peak(TokenKind::RParen, *index, input) {
        if !arguments.is_empty() {
            consume(TokenKind::Comma, index, input, file_content)?;
        }
        arguments.push(variable(index, input, file_content)?);
    }
    *index += 1;

    consume(TokenKind::LBrace, index, input, file_content)?;

    let mut statements = Vec::new();
    while !peak(TokenKind::RBrace, *index, input) {
        statements.push(statement(index, input, file_content)?);
    }
    *index += 1;

    Ok(Expression::Function {
        arguments,
        statements,
    })
}

fn block(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing block, index: {}", index);
    consume(TokenKind::LBrace, index, input, file_content)?;

    let mut statements = Vec::new();
    while !peak(TokenKind::RBrace, *index, input) {
        statements.push(statement(index, input, file_content)?);
    }
    *index += 1;

    Ok(Expression::Block { statements })
}

fn function_call(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing function call, index: {}", index);
    let ident = identifier(index, input, file_content)?;
    consume(TokenKind::LParen, index, input, file_content)?;
    let mut arguments = Vec::new();
    while !peak(TokenKind::RParen, *index, input) {
        if !arguments.is_empty() {
            consume(TokenKind::Comma, index, input, file_content)?;
        }
        arguments.push(expression(index, input, file_content)?);
    }
    *index += 1;

    Ok(Expression::FunctionCall { ident, arguments })
}

fn string_lit(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing string lit, index: {}", index);
    let token = consume(TokenKind::StringLit, index, input, file_content)?;
    let string = file_content[token.span.start + 1..token.span.end - 1].to_owned();
    Ok(Expression::StringLit { string })
}

fn int_lit(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing integer lit, index: {}", index);
    let token = consume(TokenKind::IntLit, index, input, file_content)?;
    let string = &file_content[token.span.start..token.span.end];
    match string.parse::<usize>() {
        Ok(uint) => Ok(Expression::IntLit { uint }),
        Err(err) => Err(anyhow!(
            "{}: {}",
            format_position(*index, input, file_content),
            err
        )),
    }
}

fn float_lit(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing float lit, index: {}", index);
    let token = consume(TokenKind::FloatLit, index, input, file_content)?;
    let string = &file_content[token.span.start..token.span.end];
    match string.parse::<f64>() {
        Ok(float) => Ok(Expression::FloatLit { float }),
        Err(err) => Err(anyhow!(
            "{}: {}",
            format_position(*index, input, file_content),
            err
        )),
    }
}

fn grouping(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing grouping, index: {}", index);
    consume(TokenKind::LParen, index, input, file_content)?;
    let expr = expression(index, input, file_content)?;
    consume(TokenKind::RParen, index, input, file_content)?;
    Ok(expr)
}

fn primary(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing primary, index: {}", index);
    if peak(TokenKind::r#Fn, *index, input) {
        function(index, input, file_content)
    } else if peak(TokenKind::LBrace, *index, input) {
        block(index, input, file_content)
    } else if peak(TokenKind::Ident, *index, input) {
        if peak(TokenKind::LParen, *index + 1, input) {
            function_call(index, input, file_content)
        } else {
            Ok(Expression::VariableReference {
                ident: identifier(index, input, file_content)?,
            })
        }
    } else if peak(TokenKind::StringLit, *index, input) {
        string_lit(index, input, file_content)
    } else if peak(TokenKind::IntLit, *index, input) {
        int_lit(index, input, file_content)
    } else if peak(TokenKind::FloatLit, *index, input) {
        float_lit(index, input, file_content)
    } else if peak(TokenKind::True, *index, input) {
        *index += 1;
        Ok(Expression::BooleanLit { boolean: true })
    } else if peak(TokenKind::False, *index, input) {
        *index += 1;
        Ok(Expression::BooleanLit { boolean: false })
    } else if peak(TokenKind::LParen, *index, input) {
        grouping(index, input, file_content)
    } else {
        Err(anyhow!(
            "{}: expected expression, found {}",
            format_position(*index, input, file_content),
            format_current(input.get(*index))
        ))
    }
}

fn unary(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing unary, index: {}", index);
    if peak(TokenKind::ExclamationMark, *index, input) || peak(TokenKind::Minus, *index, input) {
        let operator = UnaryOperator::from(&input[*index].kind);
        *index += 1;
        let expr = unary(index, input, file_content)?;
        Ok(Expression::Unary {
            operator,
            expression: Box::new(expr),
        })
    } else {
        primary(index, input, file_content)
    }
}

fn multiplication(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing multiplication, index: {}", index);
    let mut expr = unary(index, input, file_content)?;
    while peak(TokenKind::Asterisk, *index, input)
        || peak(TokenKind::Slash, *index, input)
        || peak(TokenKind::Percent, *index, input)
    {
        let operator = BinOperator::from(&input[*index].kind);
        *index += 1;
        let rhs = unary(index, input, file_content)?;
        expr = Expression::Binary {
            operator,
            lhs: Box::new(expr),
            rhs: Box::new(rhs),
        };
    }
    Ok(expr)
}

fn addition(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing addition, index: {}", index);
    let mut expr = multiplication(index, input, file_content)?;
    while peak(TokenKind::Plus, *index, input) || peak(TokenKind::Minus, *index, input) {
        let operator = BinOperator::from(&input[*index].kind);
        *index += 1;
        let rhs = multiplication(index, input, file_content)?;
        expr = Expression::Binary {
            operator,
            lhs: Box::new(expr),
            rhs: Box::new(rhs),
        };
    }
    Ok(expr)
}

fn equality(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing equal, index: {}", index);
    let mut expr = addition(index, input, file_content)?;
    while peak(TokenKind::GreaterThanEquals, *index, input)
        || peak(TokenKind::GreaterThan, *index, input)
        || peak(TokenKind::LessThanEquals, *index, input)
        || peak(TokenKind::LessThan, *index, input)
        || peak(TokenKind::DoubleEquals, *index, input)
    {
        let operator = BinOperator::from(&input[*index].kind);
        *index += 1;
        let rhs = addition(index, input, file_content)?;
        expr = Expression::Binary {
            operator,
            lhs: Box::new(expr),
            rhs: Box::new(rhs),
        };
    }
    Ok(expr)
}

fn logical(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing logical, index: {}", index);
    let mut expr = equality(index, input, file_content)?;
    while peak(TokenKind::DoubleAmpersand, *index, input)
        || peak(TokenKind::DoubleVerticalLine, *index, input)
    {
        let operator = BinOperator::from(&input[*index].kind);
        *index += 1;
        let rhs = equality(index, input, file_content)?;
        expr = Expression::Binary {
            operator,
            lhs: Box::new(expr),
            rhs: Box::new(rhs),
        };
    }
    Ok(expr)
}

fn expression(index: &mut usize, input: &[Token], file_content: &str) -> Result<Expression> {
    trace!("processing expression, index: {}", index);
    logical(index, input, file_content)
}

fn variable(index: &mut usize, input: &[Token], file_content: &str) -> Result<DefinitionVariable> {
    trace!("processing variable, index: {}", index);
    let mutable = peak(TokenKind::Mut, *index, input);
    if mutable {
        *index += 1;
    }

    let ident = identifier(index, input, file_content)?;

    consume(TokenKind::Colon, index, input, file_content)?;
    let r#type = r#type(index, input, file_content)?;
    Ok(DefinitionVariable {
        mutable,
        ident,
        r#type,
    })
}

fn definition(index: &mut usize, input: &[Token], file_content: &str) -> Result<Statement> {
    trace!("processing definition, index: {}", index);
    consume(TokenKind::Let, index, input, file_content)?;

    let variable = variable(index, input, file_content)?;

    consume(TokenKind::Equals, index, input, file_content)?;

    let expression = expression(index, input, file_content)?;

    consume(TokenKind::Semicolon, index, input, file_content)?;
    Ok(Statement::Definition {
        variable,
        expression,
    })
}

fn if_cond(index: &mut usize, input: &[Token], file_content: &str) -> Result<Statement> {
    trace!("processing if condition, index: {}", index);
    consume(TokenKind::If, index, input, file_content)?;

    let condition = expression(index, input, file_content)?;

    consume(TokenKind::LBrace, index, input, file_content)?;

    let mut statements = Vec::new();
    while !peak(TokenKind::RBrace, *index, input) {
        statements.push(statement(index, input, file_content)?);
    }
    *index += 1;

    let mut else_statements: Option<Vec<Statement>> = None;
    if peak(TokenKind::Else, *index, input) {
        *index += 1;

        consume(TokenKind::LBrace, index, input, file_content)?;

        else_statements = Some(Vec::new());
        while !peak(TokenKind::RBrace, *index, input) {
            else_statements
                .as_mut()
                .unwrap()
                .push(statement(index, input, file_content)?);
        }
        *index += 1;
    }

    Ok(Statement::IfCondition {
        condition,
        statements,
        else_statements,
    })
}

fn while_loop(index: &mut usize, input: &[Token], file_content: &str) -> Result<Statement> {
    trace!("processing while loop, index: {}", index);
    consume(TokenKind::While, index, input, file_content)?;

    let condition = expression(index, input, file_content)?;

    consume(TokenKind::LBrace, index, input, file_content)?;

    let mut statements = Vec::new();
    while !peak(TokenKind::RBrace, *index, input) {
        statements.push(statement(index, input, file_content)?);
    }
    *index += 1;

    Ok(Statement::WhileLoop {
        condition,
        statements,
    })
}

fn r#return(index: &mut usize, input: &[Token], file_content: &str) -> Result<Statement> {
    trace!("processing return, index: {}", index);
    consume(TokenKind::Return, index, input, file_content)?;

    let mut expr = None;
    if !peak(TokenKind::Semicolon, *index, input) {
        expr = Some(expression(index, input, file_content)?);
    }

    consume(TokenKind::Semicolon, index, input, file_content)?;
    Ok(Statement::Return { expression: expr })
}

fn assignment(index: &mut usize, input: &[Token], file_content: &str) -> Result<Statement> {
    trace!("processing assignment, index: {}", index);
    let ident = identifier(index, input, file_content)?;
    consume(TokenKind::Equals, index, input, file_content)?;
    let expression = expression(index, input, file_content)?;

    consume(TokenKind::Semicolon, index, input, file_content)?;
    Ok(Statement::Assigment { ident, expression })
}

fn statement(index: &mut usize, input: &[Token], file_content: &str) -> Result<Statement> {
    trace!("processing statement, index: {}", index);
    let statement = if peak(TokenKind::Let, *index, input) {
        definition(index, input, file_content)?
    } else if peak(TokenKind::If, *index, input) {
        if_cond(index, input, file_content)?
    } else if peak(TokenKind::While, *index, input) {
        while_loop(index, input, file_content)?
    } else if peak(TokenKind::Return, *index, input) {
        r#return(index, input, file_content)?
    } else if peak(TokenKind::Break, *index, input) {
        Statement::Break
    } else if peak(TokenKind::Continue, *index, input) {
        Statement::Continue
    } else if peak(TokenKind::Ident, *index, input) && peak(TokenKind::Equals, *index + 1, input) {
        assignment(index, input, file_content)?
    } else {
        let expression = expression(index, input, file_content)?;
        consume(TokenKind::Semicolon, index, input, file_content)?;
        Statement::Expression { expression }
    };
    Ok(statement)
}

pub fn parse(input: Vec<Token>, file_content: &str) -> Result<Vec<Statement>> {
    trace!("starting to parse {} tokens", input.len());
    let mut index = 0;
    let length = input.len();

    let mut statements = Vec::new();
    while index < length {
        statements.push(statement(&mut index, &input, file_content)?);
    }

    Ok(statements)
}
