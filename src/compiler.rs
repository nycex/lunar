use std::convert::TryInto;

use anyhow::{anyhow, Result};
use log::trace;

use crate::parser::{BinOperator, DefinitionVariable, Expression, Statement, Type, UnaryOperator};

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum OpCode {
    Constant = 0x00,
    Return = 0x01,
    Not = 0x02,
    Negate = 0x03,
    Addition = 0x04,
    Subtraction = 0x05,
    Multiplication = 0x06,
    Division = 0x07,
    Modulus = 0x08,
    GreaterEqual = 0x09,
    GreaterThan = 0x0A,
    LessEqual = 0x0B,
    LessThan = 0x0C,
    Equal = 0x0D,
    LogicalAnd = 0x0E,
    LogicalOr = 0x0F,
    Call = 0x20,
    Push = 0x21,
    Pop = 0x22,
    Load = 0x23,
    Store = 0x24,
    JumpNotTrue = 0x25,
    Jump = 0x26,
    Print = 0xF0,
}

pub type Address = u64;
pub type ConstIndex = Address;

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Float { value: f64 },
    Boolean { value: bool },
    r#String { value: String },
    Address { value: Address },
    Void,
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Value::Float { value } => write!(f, "{}", value),
            Value::Boolean { value } => write!(f, "{}", value),
            Value::r#String { value } => write!(f, "{}", value),
            Value::Address { value } => write!(f, "{}", value),
            Value::Void => write!(f, "{{}}"),
        }
    }
}

impl OpCode {
    pub const fn len(&self) -> u8 {
        const CONST_INDEX: u8 = std::mem::size_of::<ConstIndex>() as u8;
        const ADDRESS: u8 = std::mem::size_of::<Address>() as u8;
        match self {
            OpCode::Constant => 1 + CONST_INDEX,
            OpCode::Return => 1,
            OpCode::Not => 1,
            OpCode::Negate => 1,
            OpCode::Addition => 1,
            OpCode::Subtraction => 1,
            OpCode::Multiplication => 1,
            OpCode::Division => 1,
            OpCode::Modulus => 1,
            OpCode::GreaterEqual => 1,
            OpCode::GreaterThan => 1,
            OpCode::LessEqual => 1,
            OpCode::LessThan => 1,
            OpCode::Equal => 1,
            OpCode::LogicalAnd => 1,
            OpCode::LogicalOr => 1,
            OpCode::Call => 1 + ADDRESS,
            OpCode::Push => 1 + ADDRESS,
            OpCode::Pop => 1,
            OpCode::Load => 1 + ADDRESS,
            OpCode::Store => 1 + ADDRESS,
            OpCode::JumpNotTrue => 1 + ADDRESS,
            OpCode::Jump => 1 + ADDRESS,
            OpCode::Print => 1 + ADDRESS,
        }
    }
}

impl std::fmt::Debug for OpCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            OpCode::Constant => write!(f, "const"),
            OpCode::Return => write!(f, "return"),
            OpCode::Not => write!(f, "not"),
            OpCode::Negate => write!(f, "negate"),
            OpCode::Addition => write!(f, "add"),
            OpCode::Subtraction => write!(f, "sub"),
            OpCode::Multiplication => write!(f, "mul"),
            OpCode::Division => write!(f, "div"),
            OpCode::Modulus => write!(f, "mod"),
            OpCode::GreaterEqual => write!(f, "ge"),
            OpCode::GreaterThan => write!(f, "gt"),
            OpCode::LessEqual => write!(f, "le"),
            OpCode::LessThan => write!(f, "lt"),
            OpCode::Equal => write!(f, "eq"),
            OpCode::LogicalAnd => write!(f, "and"),
            OpCode::LogicalOr => write!(f, "or"),
            OpCode::Call => write!(f, "call"),
            OpCode::Push => write!(f, "push"),
            OpCode::Pop => write!(f, "pop"),
            OpCode::Load => write!(f, "load"),
            OpCode::Store => write!(f, "store"),
            OpCode::JumpNotTrue => write!(f, "jnt"),
            OpCode::Jump => write!(f, "jmp"),
            OpCode::Print => write!(f, "print"),
        }
    }
}

impl From<u8> for OpCode {
    fn from(byte: u8) -> Self {
        match byte {
            0x00 => OpCode::Constant,
            0x01 => OpCode::Return,
            0x02 => OpCode::Not,
            0x03 => OpCode::Negate,
            0x04 => OpCode::Addition,
            0x05 => OpCode::Subtraction,
            0x06 => OpCode::Multiplication,
            0x07 => OpCode::Division,
            0x08 => OpCode::Modulus,
            0x09 => OpCode::GreaterEqual,
            0x0A => OpCode::GreaterThan,
            0x0B => OpCode::LessEqual,
            0x0C => OpCode::LessThan,
            0x0D => OpCode::Equal,
            0x0E => OpCode::LogicalAnd,
            0x0F => OpCode::LogicalOr,
            0x20 => OpCode::Call,
            0x21 => OpCode::Push,
            0x22 => OpCode::Pop,
            0x23 => OpCode::Load,
            0x24 => OpCode::Store,
            0x25 => OpCode::JumpNotTrue,
            0x26 => OpCode::Jump,
            0xF0 => OpCode::Print,
            _ => panic!("unexpected byte {:#04x} in From<u8> for Opcode", byte),
        }
    }
}

#[derive(Clone)]
pub struct Chunk {
    pub source_pos_table: Vec<SourcePos>,
    pub constant_pool: Vec<Value>,
    pub code: Vec<u8>,
}

#[derive(Clone, Debug)]
pub struct SourcePos {
    line: u32,
    line_pos: u32,
}

#[derive(Clone, Debug)]
pub struct Scope<'a> {
    parent: Option<&'a Scope<'a>>,
    offset: Address,
    locals: Vec<DefinitionVariable>,
    function_definitions: Vec<FunctionDefinition>,
}

#[derive(Clone, Debug)]
struct Local {
    address: Address,
    mutable: bool,
    r#type: Type,
}

#[derive(Clone, Debug)]
struct FunctionDefinition {
    arguments: Vec<DefinitionVariable>,
    statements: Vec<Statement>,
    address_index: usize,
}

impl<'a> Scope<'a> {
    fn new(parent: Option<&'a Scope>) -> Result<Self> {
        let offset = match parent {
            Some(parent) => (parent.locals.len().saturating_sub(1)).try_into()?,
            _ => 0,
        };
        Ok(Self {
            parent,
            offset,
            locals: Vec::new(),
            function_definitions: Vec::new(),
        })
    }

    fn get_addr(&'a self, ident: impl AsRef<str>) -> Option<Address> {
        Some(self.get_local(ident)?.address)
    }

    fn get_local(&'a self, ident: impl AsRef<str>) -> Option<Local> {
        let mut current = self;
        loop {
            match current
                .locals
                .iter()
                .position(|v| v.ident == ident.as_ref())
            {
                Some(addr) => {
                    let DefinitionVariable {
                        ident: _,
                        mutable,
                        r#type,
                    } = current.locals[addr].clone();
                    return Some(Local {
                        address: (addr as Address) + current.offset,
                        mutable,
                        r#type,
                    });
                }
                None => {
                    current = current.parent?;
                }
            }
        }
    }
}

impl Chunk {
    pub fn new() -> Self {
        Self {
            source_pos_table: Vec::new(),
            constant_pool: Vec::new(),
            code: Vec::new(),
        }
    }

    pub fn write_chunk(self, mut output: impl std::io::Write) -> Result<()> {
        output.write_all(&(self.source_pos_table.len() as u64).to_le_bytes())?;
        for source_pos in self.source_pos_table {
            output.write_all(&(source_pos.line).to_le_bytes())?;
            output.write_all(&(source_pos.line_pos).to_le_bytes())?;
        }
        output.write_all(&(self.constant_pool.len() as u64).to_le_bytes())?;
        for constant in self.constant_pool {
            output.write_all(match constant {
                Value::Float { value: _ } => &[0],
                Value::Boolean { value: _ } => &[1],
                Value::r#String { value: _ } => &[2],
                Value::Address { value: _ } => &[3],
                Value::Void => &[4],
            })?;
            match constant {
                Value::Float { value } => {
                    output.write_all(&value.to_le_bytes())?;
                }
                Value::Boolean { value } => {
                    output.write_all(match value {
                        false => &[0],
                        true => &[1],
                    })?;
                }
                Value::r#String { value } => {
                    output.write_all(&(value.len() as u64).to_le_bytes())?;
                    output.write_all(value.as_bytes())?;
                }
                Value::Address { value } => {
                    output.write_all(&value.to_le_bytes())?;
                }
                Value::Void => (),
            }
        }
        output.write_all(&(self.code.len() as u64).to_le_bytes())?;
        output.write_all(self.code.as_slice())?;
        Ok(())
    }

    pub fn read_chunk(mut input: impl std::io::Read) -> Result<Self> {
        let mut eight_bytes: [u8; 8] = [0, 0, 0, 0, 0, 0, 0, 0];
        let mut four_bytes: [u8; 4] = [0, 0, 0, 0];
        let mut one_byte: [u8; 1] = [0];

        input.read_exact(&mut eight_bytes)?;
        let source_pos_table_length = u64::from_le_bytes(eight_bytes);
        let mut source_pos_table = Vec::with_capacity(source_pos_table_length.try_into()?);
        for _ in 0..source_pos_table_length {
            input.read_exact(&mut four_bytes)?;
            let line = u32::from_le_bytes(four_bytes);
            input.read_exact(&mut four_bytes)?;
            let line_pos = u32::from_le_bytes(four_bytes);
            source_pos_table.push(SourcePos { line, line_pos });
        }

        input.read_exact(&mut eight_bytes)?;
        let constant_pool_length = u64::from_le_bytes(eight_bytes);
        let mut constant_pool = Vec::with_capacity(constant_pool_length.try_into()?);
        for _ in 0..constant_pool_length {
            input.read_exact(&mut one_byte)?;
            constant_pool.push(match one_byte[0] {
                0 => {
                    input.read_exact(&mut eight_bytes)?;
                    Value::Float {
                        value: f64::from_le_bytes(eight_bytes),
                    }
                }
                1 => {
                    input.read_exact(&mut one_byte)?;
                    Value::Boolean {
                        value: match one_byte[0] {
                            0 => false,
                            1 => true,
                            _ => {
                                return Err(anyhow!(
                                    "unexpected value for boolean: {}.",
                                    one_byte[0]
                                ))
                            }
                        },
                    }
                }
                2 => {
                    input.read_exact(&mut eight_bytes)?;
                    let string_length = u64::from_le_bytes(eight_bytes);
                    let mut byte_vec = Vec::with_capacity(string_length.try_into()?);
                    for _ in 0..string_length {
                        input.read_exact(&mut one_byte)?;
                        byte_vec.push(one_byte[0]);
                    }
                    Value::r#String {
                        value: std::str::from_utf8(&byte_vec)?.to_owned(),
                    }
                }
                3 => {
                    input.read_exact(&mut eight_bytes)?;
                    Value::Address {
                        value: Address::from_le_bytes(eight_bytes),
                    }
                }
                4 => Value::Void,
                _ => return Err(anyhow!("unexpected value for type: {}.", one_byte[0])),
            });
        }

        input.read_exact(&mut eight_bytes)?;
        let code_length = u64::from_le_bytes(eight_bytes);
        let mut code = Vec::with_capacity(code_length.try_into()?);
        for _ in 0..code_length {
            input.read_exact(&mut one_byte)?;
            code.push(one_byte[0]);
        }

        Ok(Self {
            source_pos_table,
            constant_pool,
            code,
        })
    }

    fn push_opcode(&mut self, opcode: OpCode, line: u32, line_pos: u32) {
        self.code.push(opcode as u8);
        self.source_pos_table.push(SourcePos { line, line_pos });
    }

    fn push_byte(&mut self, byte: u8) {
        self.code.push(byte);
        self.source_pos_table.push(SourcePos {
            line: 0,
            line_pos: 0,
        });
    }

    fn set_byte(&mut self, index: usize, byte: u8) {
        self.code[index] = byte;
    }

    fn push_u64(&mut self, int: u64) {
        for byte in &int.to_le_bytes() {
            self.push_byte(*byte);
        }
    }

    fn set_u64(&mut self, index: usize, int: u64) {
        for (i, byte) in int.to_le_bytes().iter().enumerate() {
            self.set_byte(index + i, *byte);
        }
    }

    fn add_constant(&mut self, value: Value) -> ConstIndex {
        if let Some(index) = self.constant_pool.iter().position(|v| *v == value) {
            return index as ConstIndex;
        }
        self.constant_pool.push(value);
        (self.constant_pool.len() - 1) as ConstIndex
    }
}

impl std::fmt::Debug for Chunk {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        writeln!(f, "constant_pool: {{")?;

        for (i, constant) in self.constant_pool.iter().enumerate() {
            let prefix = if f.alternate() { "\t" } else { "" };
            writeln!(f, "{}{:#010x}: {:?}", prefix, i, constant)?;
        }

        writeln!(f, "}}\ncode: {{")?;

        let mut i = 0;
        while i < self.code.len() {
            let prefix = if f.alternate() { "\t" } else { "" };
            let opcode: OpCode = self.code[i].into();

            write!(f, "{}{:#010x}: {:?}", prefix, i, opcode)?;

            let length = opcode.len() as usize;
            let params = length - 1;
            const ADDRESS: usize = std::mem::size_of::<Address>();
            for param in 1..=(params / ADDRESS) {
                let slice = &self.code[i + param..i + param + ADDRESS];
                let address = Address::from_le_bytes(slice.try_into().unwrap());
                write!(f, " {:#010x}", address)?;
            }

            let SourcePos { line, line_pos } = self.source_pos_table[i];
            write!(f, " ({}:{})", line, line_pos)?;

            writeln!(f)?;
            i += length;
        }

        writeln!(f, "}}")?;
        Ok(())
    }
}

impl From<BinOperator> for OpCode {
    fn from(operator: BinOperator) -> Self {
        match operator {
            BinOperator::Addition => Self::Addition,
            BinOperator::Subtraction => Self::Subtraction,
            BinOperator::Multiplication => Self::Multiplication,
            BinOperator::Division => Self::Division,
            BinOperator::Modulus => Self::Modulus,
            BinOperator::GreaterEqual => Self::GreaterEqual,
            BinOperator::GreaterThan => Self::GreaterThan,
            BinOperator::LessEqual => Self::LessEqual,
            BinOperator::LessThan => Self::LessThan,
            BinOperator::Equal => Self::Equal,
            BinOperator::LogicalAnd => Self::LogicalAnd,
            BinOperator::LogicalOr => Self::LogicalOr,
        }
    }
}

impl From<UnaryOperator> for OpCode {
    fn from(operator: UnaryOperator) -> Self {
        match operator {
            UnaryOperator::Minus => Self::Negate,
            UnaryOperator::ExclamationMark => Self::Not,
        }
    }
}

fn binary(
    chunk: &mut Chunk,
    scope: &mut Scope,
    operator: BinOperator,
    lhs: Expression,
    rhs: Expression,
) -> Result<()> {
    expression(chunk, scope, lhs)?;
    expression(chunk, scope, rhs)?;
    let opcode: OpCode = operator.into();
    chunk.push_opcode(opcode, 26, 25);
    Ok(())
}

fn unary(
    chunk: &mut Chunk,
    scope: &mut Scope,
    operator: UnaryOperator,
    expr: Expression,
) -> Result<()> {
    expression(chunk, scope, expr)?;
    let opcode: OpCode = operator.into();
    chunk.push_opcode(opcode, 26, 25);
    Ok(())
}

fn int_lit(chunk: &mut Chunk, uint: usize) -> Result<()> {
    chunk.push_opcode(OpCode::Constant, 26, 25);
    // TODO: use separate value enum fields for {unsigned,} integers
    let constant = chunk.add_constant(Value::Float { value: uint as f64 });
    chunk.push_u64(constant);
    Ok(())
}

fn float_lit(chunk: &mut Chunk, float: f64) -> Result<()> {
    chunk.push_opcode(OpCode::Constant, 26, 25);
    let constant = chunk.add_constant(Value::Float { value: float });
    chunk.push_u64(constant);
    Ok(())
}

fn boolean_lit(chunk: &mut Chunk, boolean: bool) -> Result<()> {
    chunk.push_opcode(OpCode::Constant, 26, 25);
    let constant = chunk.add_constant(Value::Boolean { value: boolean });
    chunk.push_u64(constant);
    Ok(())
}

fn string_lit(chunk: &mut Chunk, string: String) -> Result<()> {
    chunk.push_opcode(OpCode::Constant, 26, 25);
    let constant = chunk.add_constant(Value::String { value: string });
    chunk.push_u64(constant);
    Ok(())
}

fn var_reference(chunk: &mut Chunk, scope: &mut Scope, ident: String) -> Result<()> {
    let address = if let Some(address) = scope.get_addr(&ident) {
        address
    } else {
        return Err(anyhow!(
            "var_reference: variable \"{}\" not found in scope.",
            ident
        ));
    };
    chunk.push_opcode(OpCode::Load, 26, 25);
    chunk.push_u64(address);
    Ok(())
}

fn definition(
    chunk: &mut Chunk,
    scope: &mut Scope,
    variable: DefinitionVariable,
    expr: Expression,
) -> Result<()> {
    let address = if scope.get_addr(&variable.ident).is_some() {
        return Err(anyhow!(
            "definition: variable \"{}\" already declared in scope.",
            variable.ident
        ));
    } else {
        scope.locals.push(variable);
        (scope.locals.len() as Address) - 1 + scope.offset
    };
    expression(chunk, scope, expr)?;
    chunk.push_opcode(OpCode::Store, 26, 25);
    chunk.push_u64(address);
    Ok(())
}

fn assignment(chunk: &mut Chunk, scope: &mut Scope, ident: String, expr: Expression) -> Result<()> {
    let address = if let Some(Local {
        address,
        mutable,
        r#type: _,
    }) = scope.get_local(&ident)
    {
        if mutable {
            address
        } else {
            return Err(anyhow!("assignment: variable \"{}\" is immutable.", ident));
        }
    } else {
        return Err(anyhow!(
            "assignment: variable \"{}\" not found in scope.",
            ident
        ));
    };
    expression(chunk, scope, expr)?;
    chunk.push_opcode(OpCode::Store, 26, 25);
    chunk.push_u64(address);
    Ok(())
}

// takes local index instead of instruction index
fn function_call(
    chunk: &mut Chunk,
    scope: &mut Scope,
    ident: String,
    arguments: Vec<Expression>,
) -> Result<()> {
    let length = arguments.len() as u64;
    for argument in arguments {
        expression(chunk, scope, argument)?;
    }
    // TODO: improve builtin functions
    if ident == "print" {
        chunk.push_opcode(OpCode::Print, 26, 25);
        chunk.push_u64(length);
        return Ok(());
    }
    let address = if let Some(address) = scope.get_addr(&ident) {
        address
    } else {
        return Err(anyhow!(
            "function_call: function \"{}\" not found in scope.",
            ident
        ));
    };
    chunk.push_opcode(OpCode::Call, 26, 25);
    chunk.push_u64(address);
    Ok(())
}

fn function_definition(
    chunk: &mut Chunk,
    scope: &mut Scope,
    arguments: Vec<DefinitionVariable>,
    statements: Vec<Statement>,
) -> Result<()> {
    chunk.push_opcode(OpCode::Push, 26, 25);
    let address_index = chunk.code.len();
    chunk.push_u64(0xFFFFFFFFFFFFFFFF);
    let fn_def = FunctionDefinition {
        arguments,
        statements,
        address_index,
    };
    scope.function_definitions.push(fn_def);

    Ok(())
}

fn block(chunk: &mut Chunk, scope: &mut Scope, stats: Vec<Statement>) -> Result<()> {
    let mut block_scope = Scope::new(Some(scope))?;
    for stat in stats {
        statement(chunk, &mut block_scope, stat)?;
    }
    chunk.push_opcode(OpCode::Constant, 26, 25);
    let constant = chunk.add_constant(Value::Void);
    chunk.push_u64(constant);
    Ok(())
}

fn expression(chunk: &mut Chunk, scope: &mut Scope, expr: Expression) -> Result<()> {
    trace!("compiling expression {:?}", expr);
    match expr {
        Expression::Binary { operator, lhs, rhs } => binary(chunk, scope, operator, *lhs, *rhs),
        Expression::Unary {
            operator,
            expression: expr,
        } => unary(chunk, scope, operator, *expr),
        Expression::IntLit { uint } => int_lit(chunk, uint),
        Expression::FloatLit { float } => float_lit(chunk, float),
        Expression::BooleanLit { boolean } => boolean_lit(chunk, boolean),
        Expression::StringLit { string } => string_lit(chunk, string),
        Expression::VariableReference { ident } => var_reference(chunk, scope, ident),
        Expression::FunctionCall { ident, arguments } => {
            function_call(chunk, scope, ident, arguments)
        }
        Expression::Function {
            arguments,
            statements,
        } => function_definition(chunk, scope, arguments, statements),
        Expression::Block { statements } => block(chunk, scope, statements),
    }
}

fn if_condition(
    chunk: &mut Chunk,
    scope: &mut Scope,
    condition: Expression,
    statements: Vec<Statement>,
    else_statements: Option<Vec<Statement>>,
) -> Result<()> {
    expression(chunk, scope, condition)?;
    chunk.push_opcode(OpCode::JumpNotTrue, 26, 25);
    let not_true_jump_addr_index = chunk.code.len();
    chunk.push_u64(0xFFFFFFFFFFFFFFFF);
    let mut if_scope = Scope::new(Some(scope))?;
    for stat in statements {
        statement(chunk, &mut if_scope, stat)?;
    }
    let mut if_branch_jump_addr_index = None;
    let mut after_if_branch_jump = None;
    if let Some(else_statements) = else_statements {
        chunk.push_opcode(OpCode::Jump, 26, 25);
        if_branch_jump_addr_index = Some(chunk.code.len());
        chunk.push_u64(0xFFFFFFFFFFFFFFFF);
        after_if_branch_jump = Some(chunk.code.len().try_into()?);
        for else_stat in else_statements {
            statement(chunk, scope, else_stat)?;
        }
    }
    let address = chunk.code.len().try_into()?;
    if let (Some(if_branch_jump_addr_index), Some(after_if_branch_jump)) =
        (if_branch_jump_addr_index, after_if_branch_jump)
    {
        chunk.set_u64(if_branch_jump_addr_index, address);
        chunk.set_u64(not_true_jump_addr_index, after_if_branch_jump);
    } else {
        chunk.set_u64(not_true_jump_addr_index, address);
    }
    Ok(())
}

fn while_loop(
    chunk: &mut Chunk,
    scope: &mut Scope,
    condition: Expression,
    statements: Vec<Statement>,
) -> Result<()> {
    let cond_address = chunk.code.len();
    expression(chunk, scope, condition)?;
    chunk.push_opcode(OpCode::JumpNotTrue, 26, 25);
    let address_index = chunk.code.len();
    chunk.push_u64(0xFFFFFFFFFFFFFFFF);
    let mut if_scope = Scope::new(Some(scope))?;
    for stat in statements {
        statement(chunk, &mut if_scope, stat)?;
    }
    chunk.push_opcode(OpCode::Jump, 26, 25);
    chunk.push_u64(cond_address.try_into()?);
    let after_address = chunk.code.len();
    chunk.set_u64(address_index, after_address.try_into()?);
    Ok(())
}

fn r#break(chunk: &mut Chunk, scope: &mut Scope) -> Result<()> {
    let _ = (chunk, scope);
    todo!("implement breaks")
}

fn r#continue(chunk: &mut Chunk, scope: &mut Scope) -> Result<()> {
    let _ = (chunk, scope);
    todo!("implement continues")
}

fn statement(chunk: &mut Chunk, scope: &mut Scope, stat: Statement) -> Result<()> {
    trace!("compiling statement {:?}", stat);
    match stat {
        Statement::Expression { expression: expr } => {
            expression(chunk, scope, expr)?;
            chunk.push_opcode(OpCode::Pop, 26, 25);
        }
        Statement::Return { expression: expr } => {
            if let Some(expr) = expr {
                expression(chunk, scope, expr)?;
            } else {
                chunk.push_opcode(OpCode::Constant, 26, 25);
                let constant = chunk.add_constant(Value::Void);
                chunk.push_u64(constant);
            }
            chunk.push_opcode(OpCode::Return, 26, 25);
        }
        Statement::Definition {
            variable,
            expression: expr,
        } => definition(chunk, scope, variable, expr)?,
        Statement::Assigment {
            ident,
            expression: expr,
        } => assignment(chunk, scope, ident, expr)?,
        Statement::IfCondition {
            condition,
            statements,
            else_statements,
        } => if_condition(chunk, scope, condition, statements, else_statements)?,
        Statement::WhileLoop {
            condition,
            statements,
        } => while_loop(chunk, scope, condition, statements)?,
        Statement::Break => r#break(chunk, scope)?,
        Statement::Continue => r#continue(chunk, scope)?,
    };
    Ok(())
}

fn function_definitions(chunk: &mut Chunk, scope: &mut Scope) -> Result<()> {
    for function_definition in scope.function_definitions.clone() {
        let address = chunk.code.len();
        chunk.set_u64(function_definition.address_index, address.try_into()?);
        let mut fn_scope = Scope::new(Some(scope))?;
        for argument in function_definition.arguments.into_iter().rev() {
            let address = if fn_scope.get_addr(&argument.ident).is_some() {
                return Err(anyhow!(
                    "function_definition: argument: variable \"{}\" already declared in scope.",
                    argument.ident
                ));
            } else {
                fn_scope.locals.push(argument);
                (fn_scope.locals.len() as Address) - 1 + fn_scope.offset
            };
            chunk.push_opcode(OpCode::Store, 26, 25);
            chunk.push_u64(address);
        }
        for stat in function_definition.statements {
            statement(chunk, &mut fn_scope, stat)?;
        }
        chunk.push_opcode(OpCode::Constant, 26, 25);
        let constant = chunk.add_constant(Value::Void);
        chunk.push_u64(constant);
        chunk.push_opcode(OpCode::Return, 26, 25);
    }
    Ok(())
}

pub fn compile(statements: Vec<Statement>) -> Result<Chunk> {
    trace!("starting to compile {} statements", statements.len());
    let mut chunk = Chunk::new();
    let mut scope = Scope::new(None)?;

    for stat in statements {
        statement(&mut chunk, &mut scope, stat)?;
    }
    chunk.push_opcode(OpCode::Return, 26, 25); // TODO: actual line numbers
    function_definitions(&mut chunk, &mut scope)?;

    assert!(chunk.source_pos_table.len() == chunk.code.len());

    Ok(chunk)
}
