use anyhow::{anyhow, Result};
use log::trace;

#[derive(Clone, Debug)]
pub struct Token {
    pub kind: TokenKind,
    pub span: Span,
}

impl std::fmt::Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.kind)
    }
}

#[derive(Clone, Debug)]
pub struct Span {
    pub start: usize,
    pub end: usize,
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind {
    Ident,

    // keywords
    Let,
    Mut,
    If,
    Else,
    While,
    Return,
    Break,
    Continue,
    r#Fn,
    r#String,
    USize,
    ISize,
    Float,
    Boolean,
    Void,

    // literals
    StringLit,
    IntLit,
    FloatLit,
    True,
    False,

    Colon,
    LBrace,
    RBrace,
    LParen,
    RParen,
    Semicolon,
    Comma,
    Plus,
    Minus,
    Asterisk,
    Slash,
    Percent,
    DoubleEquals,
    Equals,
    GreaterThanEquals,
    DoubleGreaterThan,
    GreaterThan,
    LessThanEquals,
    DoubleLessThan,
    LessThan,
    DoubleAmpersand,
    Ampersand,
    DoubleVerticalLine,
    VerticalLine,
    Caret,
    Tilde,
    ExclamationMark,
}

impl std::fmt::Display for TokenKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TokenKind::Ident => write!(f, "identifier"),
            TokenKind::Let => write!(f, "`let`"),
            TokenKind::Mut => write!(f, "`mut`"),
            TokenKind::If => write!(f, "`if`"),
            TokenKind::Else => write!(f, "`else`"),
            TokenKind::While => write!(f, "`while`"),
            TokenKind::Return => write!(f, "`return`"),
            TokenKind::Break => write!(f, "`break`"),
            TokenKind::Continue => write!(f, "`continue`"),
            TokenKind::r#Fn => write!(f, "`fn`"),
            TokenKind::r#String => write!(f, "`string`"),
            TokenKind::USize => write!(f, "`usize`"),
            TokenKind::ISize => write!(f, "`isize`"),
            TokenKind::Float => write!(f, "`float`"),
            TokenKind::Boolean => write!(f, "`boolean`"),
            TokenKind::Void => write!(f, "void"),
            TokenKind::StringLit => write!(f, "string literal"),
            TokenKind::IntLit => write!(f, "integer literal"),
            TokenKind::FloatLit => write!(f, "float literal"),
            TokenKind::True => write!(f, "true"),
            TokenKind::False => write!(f, "false"),
            TokenKind::Colon => write!(f, "`:`"),
            TokenKind::LBrace => write!(f, "`{{`"),
            TokenKind::RBrace => write!(f, "`}}`"),
            TokenKind::LParen => write!(f, "`(`"),
            TokenKind::RParen => write!(f, "`)`"),
            TokenKind::Semicolon => write!(f, "`;`"),
            TokenKind::Comma => write!(f, "`,`"),
            TokenKind::Plus => write!(f, "`+`"),
            TokenKind::Minus => write!(f, "`-`"),
            TokenKind::Asterisk => write!(f, "`*`"),
            TokenKind::Slash => write!(f, "`/`"),
            TokenKind::Percent => write!(f, "`%`"),
            TokenKind::DoubleEquals => write!(f, "`==`"),
            TokenKind::Equals => write!(f, "`=`"),
            TokenKind::GreaterThanEquals => write!(f, "`>=`"),
            TokenKind::DoubleGreaterThan => write!(f, "`>>`"),
            TokenKind::GreaterThan => write!(f, "`>`"),
            TokenKind::LessThanEquals => write!(f, "`<=`"),
            TokenKind::DoubleLessThan => write!(f, "`<<`"),
            TokenKind::LessThan => write!(f, "`<`"),
            TokenKind::DoubleAmpersand => write!(f, "`&&`"),
            TokenKind::Ampersand => write!(f, "`&`"),
            TokenKind::DoubleVerticalLine => write!(f, "`||`"),
            TokenKind::VerticalLine => write!(f, "`|`"),
            TokenKind::Caret => write!(f, "`^`"),
            TokenKind::Tilde => write!(f, "`~`"),
            TokenKind::ExclamationMark => write!(f, "`!`"),
        }
    }
}

fn format_position(input: &str, index: usize) -> String {
    let mut line = 1;
    let mut char_pos = 1;
    for (i, chr) in input.char_indices().take(index) {
        match chr {
            '\n' => {
                line += 1;
                char_pos = 1;
            }
            '\r' => {
                if input.chars().nth(i + 1) != Some('\n') {
                    line += 1;
                    char_pos = 1;
                }
            }
            _ => {
                char_pos += 1;
            }
        }
    }
    format!("line {}, pos: {}", line, char_pos)
}

fn skip_ignorable(input: &str, index: &mut usize) {
    loop {
        if let Some(current_char) = input.chars().nth(*index) {
            if current_char.is_ascii_whitespace() {
                *index += 1;
            } else if current_char == '/' && input.chars().nth(*index + 1) == Some('/') {
                while input.chars().nth(*index) != Some('\n') {
                    *index += 1;
                }
            } else {
                return;
            }
        } else {
            return;
        }
    }
}

fn indent_or_keyword(current_char: char, input: &str, index: usize) -> Result<(TokenKind, usize)> {
    if !(current_char.is_alphabetic() || current_char == '_') {
        return Err(anyhow!(
            "{}: char '{}' is not allowed at the start of a keyword/identifier.",
            format_position(input, index),
            current_char
        ));
    }
    let mut cur_index = index + 1;
    while let Some(chr) = (*input).chars().nth(cur_index) {
        if !(chr.is_alphanumeric() || chr == '_') {
            break;
        }
        cur_index += 1;
    }

    let string = &input[index..cur_index];
    trace!("lexing keyword or ident \"{}\"", string);

    let kind = match string {
        "let" => TokenKind::Let,
        "mut" => TokenKind::Mut,
        "if" => TokenKind::If,
        "else" => TokenKind::Else,
        "while" => TokenKind::While,
        "fn" => TokenKind::r#Fn,
        "return" => TokenKind::Return,
        "break" => TokenKind::Break,
        "continue" => TokenKind::Continue,
        "string" => TokenKind::r#String,
        "usize" => TokenKind::USize,
        "isize" => TokenKind::ISize,
        "float" => TokenKind::Float,
        "boolean" => TokenKind::Boolean,
        "void" => TokenKind::Void,
        "true" => TokenKind::True,
        "false" => TokenKind::False,
        _ => TokenKind::Ident,
    };
    let len = cur_index - index;

    Ok((kind, len))
}

fn string(input: &str, index: usize) -> Result<(TokenKind, usize)> {
    let len_opt = input[index + 1..].chars().position(|chr| chr == '"');
    if let Some(len) = len_opt {
        let len = len + 2; // add '"' from the beginning and position to len "conversion"
        trace!("lexing string \"{}\"", &input[index + 1..index + len - 1]);
        Ok((TokenKind::StringLit, len))
    } else {
        Err(anyhow!(
            "{}: strings have to be terminated with '\"'",
            format_position(input, index),
        ))
    }
}

fn number(input: &str, index: usize) -> (TokenKind, usize) {
    let mut cur_index = index;
    let mut had_point = false;
    while let Some(chr) = (*input).chars().nth(cur_index) {
        if !(chr.is_numeric() || !had_point && chr == '.') {
            break;
        } else if chr == '.' {
            had_point = true;
        }
        cur_index += 1;
    }

    trace!("lexing number \"{}\"", &input[index..cur_index]);

    let len = cur_index - index;

    let token_kind = if had_point {
        TokenKind::FloatLit
    } else {
        TokenKind::IntLit
    };

    (token_kind, len)
}

fn equals(input: &str, index: usize) -> (TokenKind, usize) {
    if let Some('=') = input.chars().nth(index + 1) {
        (TokenKind::DoubleEquals, 2)
    } else {
        (TokenKind::Equals, 1)
    }
}

fn ampersand(input: &str, index: usize) -> (TokenKind, usize) {
    if let Some('&') = input.chars().nth(index + 1) {
        (TokenKind::DoubleAmpersand, 2)
    } else {
        (TokenKind::Ampersand, 1)
    }
}

fn vertical_line(input: &str, index: usize) -> (TokenKind, usize) {
    if let Some('|') = input.chars().nth(index + 1) {
        (TokenKind::DoubleVerticalLine, 2)
    } else {
        (TokenKind::VerticalLine, 1)
    }
}

fn greater_than(input: &str, index: usize) -> (TokenKind, usize) {
    if let Some('=') = input.chars().nth(index + 1) {
        (TokenKind::GreaterThanEquals, 2)
    } else if let Some('>') = input.chars().nth(index + 1) {
        (TokenKind::DoubleGreaterThan, 2)
    } else {
        (TokenKind::GreaterThan, 1)
    }
}

fn less_than(input: &str, index: usize) -> (TokenKind, usize) {
    if let Some('=') = input.chars().nth(index + 1) {
        (TokenKind::LessThanEquals, 2)
    } else if let Some('<') = input.chars().nth(index + 1) {
        (TokenKind::DoubleLessThan, 2)
    } else {
        (TokenKind::LessThan, 1)
    }
}

pub fn lex(input: &str) -> Result<Vec<Token>> {
    trace!("starting to lex {} byte-long string", input.len());
    let mut tokens: Vec<Token> = Vec::new();

    let mut index = 0;
    let length = input.len();

    while index < length {
        skip_ignorable(&input, &mut index);
        let prev_index = index;
        if let Some(current_char) = input.chars().nth(index) {
            let (kind, len) = match current_char {
                ':' => (TokenKind::Colon, 1),
                ';' => (TokenKind::Semicolon, 1),
                ',' => (TokenKind::Comma, 1),
                '{' => (TokenKind::LBrace, 1),
                '}' => (TokenKind::RBrace, 1),
                '(' => (TokenKind::LParen, 1),
                ')' => (TokenKind::RParen, 1),
                '+' => (TokenKind::Plus, 1),
                '-' => (TokenKind::Minus, 1),
                '*' => (TokenKind::Asterisk, 1),
                '/' => (TokenKind::Slash, 1),
                '%' => (TokenKind::Percent, 1),
                '>' => greater_than(&input, index),
                '<' => less_than(&input, index),
                '=' => equals(&input, index),
                '&' => ampersand(&input, index),
                '|' => vertical_line(&input, index),
                '^' => (TokenKind::Caret, 1),
                '~' => (TokenKind::Tilde, 1),
                '!' => (TokenKind::ExclamationMark, 1),
                '"' => string(&input, index)?,
                '0'..='9' => number(&input, index),
                _ => indent_or_keyword(current_char, &input, index)?,
            };
            index += len;

            tokens.push(Token {
                kind,
                span: Span {
                    start: prev_index,
                    end: index,
                },
            });
        }
    }

    Ok(tokens)
}
